import pytest
from Extended_Widgets.implementation.PlotItemUtils import PlotItemUtils


def __point(x, y):
    return {
        'x': x,
        'y': y
    }


def test_binary_search_1():
    x_list = list(range(0, 101))
    line = 5.6
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == 5
    assert result['after'] == 6


def test_binary_search_2():
    x_list = list(range(1, 101))
    line = 5.6
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == 4
    assert result['after'] == 5


def test_binary_search_3():
    x_list = list(range(0, 101))
    line = 5
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == 5
    assert result['after'] == 5


def test_binary_search_4():
    x_list = list(range(0, 2))
    line = 0.5
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == 0
    assert result['after'] == 1


def test_binary_search_5():
    x_list = list(range(0, 2))
    line = 0
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == 0
    assert result['after'] == 0


def test_binary_search_6():
    x_list = list(range(0, 101))
    line = -1
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == -1


def test_binary_search_7():
    x_list = list(range(0, 101))
    line = 101
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == 100
    assert result['after'] == -1


def test_binary_search_8():
    x_list = []
    line = 11
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == -1
    assert result['after'] == -1


def test_binary_search_10():
    x_list = [0.0, 0.25, 0.5, 0.75, 1.0]
    line = 0.0
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == 0
    assert result['after'] == 0


def test_binary_search_11():
    x_list = [0.0, 0.25, 0.5, 0.75, 1.0]
    line = 0.25
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == 1
    assert result['after'] == 1


def test_binary_search_12():
    x_list = [0.0, 0.25, 0.5, 0.75, 1.0]
    line = 0.5
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == 2
    assert result['after'] == 2


def test_binary_search_13():
    x_list = [0.0, 0.25, 0.5, 0.75, 1.0]
    line = 0.75
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == 3
    assert result['after'] == 3


def test_binary_search_14():
    x_list = [0.0, 0.25, 0.5, 0.75, 1.0]
    line = 1.0
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == 4
    assert result['after'] == 4


def test_binary_search_15():
    x_list = [0.0]
    line = 0.0
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == 0
    assert result['after'] == 0


def test_binary_search_16():
    x_list = [0.0]
    line = -1.0
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == -1
    assert result['after'] == 0


def test_binary_search_17():
    x_list = [0.0]
    line = 1.0
    result = PlotItemUtils.bin_search_surrounding_points(x_list, line)
    assert result['before'] == 0
    assert result['after'] == -1


def test_calc_intersection_1():
    p1 = __point(0, 0)
    p2 = __point(2, 2)
    line = 1
    result = PlotItemUtils.calc_intersection(p1, p2, line)
    assert result['x'] == 1
    assert result['y'] == 1


def test_calc_intersection_2():
    p1 = __point(0, 0)
    p2 = __point(2, 1)
    line = 1
    result = PlotItemUtils.calc_intersection(p1, p2, line)
    assert result['x'] == 1
    assert result['y'] == 0.5


def test_calc_intersection_3():
    p1 = __point(100.0, 100.0)
    p2 = __point(200.0, 200.0)
    line = 150.0
    result = PlotItemUtils.calc_intersection(p1, p2, line)
    assert result['x'] == 150.0
    assert result['y'] == 150.0


def test_calc_intersection_4():
    p1 = __point(-100.0, -100.0)
    p2 = __point(100.0, 100.0)
    line = 0
    result = PlotItemUtils.calc_intersection(p1, p2, line)
    assert result['x'] == 0.0
    assert result['y'] == 0.0


def test_calc_intersection_5():
    p1 = __point(-200.0, -200.0)
    p2 = __point(-100.0, -100.0)
    line = -150.0
    result = PlotItemUtils.calc_intersection(p1, p2, line)
    assert result['x'] == -150.0
    assert result['y'] == -150.0


def test_calc_intersection_6():
    p1 = __point(0, 0)
    p2 = __point(0, 0)
    line = 0
    result = PlotItemUtils.calc_intersection(p1, p2, line)
    assert result['x'] == 0
    assert result['y'] == 0


def test_calc_intersection_7():
    p1 = __point(0, 0)
    p2 = __point(2, 2)
    line = -1
    assert PlotItemUtils.calc_intersection(p1, p2, line) is None


def test_calc_intersection_8():
    p1 = __point(0, 0)
    p2 = __point(2, 2)
    line = 3
    assert PlotItemUtils.calc_intersection(p1, p2, line) is None


def test_calc_intersection_9():
    p1 = __point(2, 2)
    p2 = __point(0, 0)
    line = 1
    result = PlotItemUtils.calc_intersection(p1, p2, line)
    assert result['x'] == 1
    assert result['y'] == 1


def test_calc_intersection_10():
    p1 = __point(2, 2)
    p2 = __point(0, 0)
    line = 2
    result = PlotItemUtils.calc_intersection(p1, p2, line)
    assert result['x'] == 2
    assert result['y'] == 2


def test_calc_intersection_11():
    p1 = __point(2, 2)
    p2 = __point(0, 0)
    line = 3
    assert PlotItemUtils.calc_intersection(p1, p2, line) is None


def test_calc_intersection_12():
    p1 = __point(2, 2)
    p2 = __point(0, 0)
    line = -1
    assert PlotItemUtils.calc_intersection(p1, p2, line) is None


def test_calc_intersection_13():
    p1 = {'a': 0, 'b': 0}
    p2 = {'a': 2, 'b': 2}
    line = 1
    with pytest.raises(TypeError):
        PlotItemUtils.calc_intersection(p1, p2, line)


def test_intersect_1():
    x = [0, 1, 2, 3, 4]
    y = [0, 1, 2, 3, 4]
    points = __point(x, y)
    line = 1.5
    result = PlotItemUtils.intersect(points, line)
    assert result['last_before_index'] == 1
    assert result['first_after_index'] == 2
    assert result['intersection']['x'] == 1.5
    assert result['intersection']['y'] == 1.5


def test_intersect_2():
    x = [0, 1, 2, 3, 4]
    y = [0, 1, 2, 3, 4]
    points = __point(x, y)
    line = 0
    result = PlotItemUtils.intersect(points, line)
    assert result['last_before_index'] == 0
    assert result['first_after_index'] == 0
    assert result['intersection']['x'] == 0
    assert result['intersection']['y'] == 0


def test_intersect_3():
    x = [0, 1, 2, 3, 4]
    y = [0, 1, 2, 3, 4]
    points = __point(x, y)
    line = -1
    result = PlotItemUtils.intersect(points, line)
    assert result['last_before_index'] == -1
    assert result['first_after_index'] == 0
    assert result['intersection'] is None


def test_intersect_4():
    x = [0, 1, 2, 3, 4]
    y = [0, 1, 2, 3, 4]
    points = __point(x, y)
    line = 5
    result = PlotItemUtils.intersect(points, line)
    assert result['last_before_index'] == 4
    assert result['first_after_index'] == -1
    assert result['intersection'] is None


def test_intersect_5():
    x = [0]
    y = [0]
    points = __point(x, y)
    line = 1
    result = PlotItemUtils.intersect(points, line)
    assert result['last_before_index'] == 0
    assert result['first_after_index'] == -1
    assert result['intersection'] is None


def test_intersect_6():
    x = [0]
    y = [0]
    points = __point(x, y)
    line = -1
    result = PlotItemUtils.intersect(points, line)
    assert result['last_before_index'] == -1
    assert result['first_after_index'] == 0
    assert result['intersection'] is None


def test_intersect_7():
    x = []
    y = []
    points = __point(x, y)
    line = 0
    result = PlotItemUtils.intersect(points, line)
    assert result['last_before_index'] == -1
    assert result['first_after_index'] == -1
    assert result['intersection'] is None
