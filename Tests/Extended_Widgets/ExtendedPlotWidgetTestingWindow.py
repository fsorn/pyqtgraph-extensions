from PyQt5.QtWidgets import QMainWindow, QWidget, QGridLayout, QApplication
from typing import Dict, Union
from Tests.Mock_Connection.TimeSourceMock import TimingSourceMock
from Tests.Mock_Connection.DataSourceMock import DataSourceMock
from Extended_Widgets.implementation.ExtendedPlotWidget import ExtendedPlotWidget as Epw
from Extended_Widgets.implementation.ExtendedPlotWidget import PlotWidgetStyle as Pws


class ExtendedPlotWidgetTestingWindow(QMainWindow):

    def __init__(self, plot_config: Dict[str, Union[str, Pws, int, float]]):
        super(ExtendedPlotWidgetTestingWindow, self).__init__()
        # Two Threads for Time and Data updates
        self.time_source_mock = TimingSourceMock()
        self.data_source_mock = DataSourceMock()
        self.plot: Epw = Epw(timing_source=self.time_source_mock, data_source=self.data_source_mock, config=plot_config)
        self.resize(800, 600)
        main_container = QWidget()
        self.setCentralWidget(main_container)
        main_layout = QGridLayout()
        main_container.setLayout(main_layout)
        main_layout.addWidget(self.plot)