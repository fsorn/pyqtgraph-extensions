import itertools
from datetime import datetime
from pyqtgraph import InfiniteLine, PlotDataItem
from Tests.Extended_Widgets.ExtendedPlotWidgetTestingWindow import ExtendedPlotWidgetTestingWindow
from Extended_Widgets.implementation.ExtendedPlotWidget import ExtendedPlotWidget as Epw
from Extended_Widgets.implementation.ScrollingPlotItem import ScrollingPlotItem as Spi
from Extended_Widgets.implementation.SlidingPointerPlotItem import SlidingPointerPlotItem as Sppi
from Extended_Widgets.implementation.ExtendedPlotWidget import PlotWidgetStyle as Pws


def test_widget_configuration(qtbot):
    """
    Iterate through the possible combinations of parameters when creating the
    ExtendedPlotWidget and check if all elements are created as expected.
    """
    cycle_sizes = [2, 100]
    plotting_styles = [[Pws.SCROLLING_PLOT, Spi], [Pws.SLIDING_POINTER, Sppi]]
    time_progress_line_values = [False, True]
    v_draw_line_values = [False, True]
    h_draw_line_values = [False, True]
    draw_point_values = [False, True]
    combined = itertools.product(cycle_sizes, plotting_styles, time_progress_line_values, v_draw_line_values,
                                 h_draw_line_values, draw_point_values)
    for item in combined:
        plot_config = {
            'cycle_size': item[0],
            'plotting_style': item[1][0],
            'time_progress_line': item[2],
            'v_draw_line': item[3],
            'h_draw_line': item[4],
            'draw_point': item[5]
        }
        window = ExtendedPlotWidgetTestingWindow(plot_config)
        window.show()
        qtbot.addWidget(window)
        # Decorators need one time update for the initial drawing of the decorators, f.e. time_line
        # can not be drawn without a first timing update, same with decorators for appended data
        time_1 = 0.0
        data_x = 0.75
        data_y = 5.25
        time_2 = 1.0
        window.time_source_mock.create_new_value(time_1)
        window.data_source_mock.create_new_value(data_x, data_y)
        window.time_source_mock.create_new_value(time_2)
        assert isinstance(window.plot, Epw)
        assert isinstance(window.plot.plotItem, item[1][1])
        pi = window.plot.plotItem
        assert ((isinstance(pi.time_line, InfiniteLine)) if item[2] else (pi.time_line is None))
        assert ((pi.time_line.value() == time_2) if item[2] else True)
        assert ((isinstance(pi.plotting_line_v, InfiniteLine)) if item[3] else (pi.plotting_line_v is None))
        assert ((pi.plotting_line_v.value() == data_x) if item[3] else True)
        assert ((isinstance(pi.plotting_line_h, InfiniteLine)) if item[4] else (pi.plotting_line_h is None))
        assert ((pi.plotting_line_h.value() == data_y) if item[4] else True)
        assert ((isinstance(pi.plotting_line_point, PlotDataItem)) if item[5] else (pi.plotting_line_point is None))
        assert check_axis_strings(pi, item[1][0])
        if item[5]:
            assert list(pi.plotting_line_point.getData()[0]) == [data_x]
            assert list(pi.plotting_line_point.getData()[1]) == [data_y]


def check_axis_strings(pi: Spi, style: Pws) -> bool:
    values = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
    if style == Pws.SLIDING_POINTER:
        expected = ['0.0s', '+1.0s', '+2.0s', '+3.0s', '+4.0s', '+5.0s']
        return pi.getAxis('bottom').tickStrings(values, 1, 1) == expected
    elif style == Pws.SCROLLING_PLOT:
        expected = [datetime.fromtimestamp(value).strftime('%H:%M:%S') for value in values]
        return pi.getAxis('bottom').tickStrings(values, 1, 1) == expected
