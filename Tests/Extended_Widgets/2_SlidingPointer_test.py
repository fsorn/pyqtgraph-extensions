from typing import List, Dict
import numpy
import json
from Tests.Mock_Connection.TimeSourceMock import TimingSourceMock
from Tests.Mock_Connection.DataSourceMock import DataSourceMock
from Tests.Extended_Widgets.ExtendedPlotWidgetTestingWindow import ExtendedPlotWidgetTestingWindow
from Extended_Widgets.implementation.SlidingPointerPlotItem import SlidingPointerPlotItem as Sppi
from Extended_Widgets.implementation.ExtendedPlotWidget import PlotWidgetStyle as Pws

"""
Testing for gradually plotting with the ExtendedPlotWidget.
This includes also the ExtendedPlotItem classes.
"""


# Simply append data to plot and see if they Signals trigger right slots
def test_data_append(qtbot):
    timestamps = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]
    datasets_delta = 0.25
    current_dataset_timestamp = 0.0
    window = __prepare_sliding_pointer_plot_test_window(qtbot, 5)
    __simple_linear_update(window.time_source_mock,
                           window.data_source_mock,
                           timestamps,
                           current_dataset_timestamp,
                           datasets_delta)
    expected_data = list(numpy.arange(current_dataset_timestamp, timestamps[-1] + datasets_delta, datasets_delta))
    assert window.plot.plotItem.last_timestamp == 10.0
    assert window.plot.plotItem.full_curve_buffer['timestamps'] == expected_data
    assert window.plot.plotItem.full_curve_buffer['y'] == expected_data


def test_plot_after_first_timing_update(qtbot):
    window = __prepare_sliding_pointer_plot_test_window(qtbot, 2)
    plot_item = window.plot.plotItem
    time = window.time_source_mock
    data = window.data_source_mock
    expected_old = __curve_dict(timestamp=[], x=[], y=[])
    # Start actual testing
    time.create_new_value(0.0)
    data.create_new_value(0.5, 0.5)
    expected_full = __curve_dict(timestamp=[0.5], y=[0.5])
    expected_new = __curve_dict(timestamp=[0.5], x=[0.5], y=[0.5])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item)


def test_plot_before_first_timing_update(qtbot):
    window = __prepare_sliding_pointer_plot_test_window(qtbot, 2)
    plot_item = window.plot.plotItem
    time = window.time_source_mock
    data = window.data_source_mock
    expected_old = __curve_dict(timestamp=[], x=[], y=[])
    expected_new = __curve_dict(timestamp=[], x=[], y=[])
    # Start actual testing
    data.create_new_value(0.5, 0.5)
    expected_full = __curve_dict(timestamp=[0.5], y=[0.5])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    time.create_new_value(0.0)
    expected_new = __curve_dict(timestamp=[0.5], x=[0.5], y=[0.5])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item)

# ================================================================
# Clipping is happening in mainly 4 areas:
# 1. Clipping of points with timestamp that is bigger than the
#    current time at the vertical line representing the current time
# 2. Clipping of the old curve at the line representing the current
#    time
# 3. Clipping of the new and old curve at the cycle end
# ================================================================


def test_clipping_1(qtbot):
    """
    Test the handling of data with timestamps that are bigger than the
    currently known one. The expected behavior is that the curve is clipped
    at the current timeline and as time progresses, more of the "future"
    line is revealed.
    """
    window = __prepare_sliding_pointer_plot_test_window(qtbot, 2)
    plot_item = window.plot.plotItem
    time = window.time_source_mock
    data = window.data_source_mock
    expected_old = __curve_dict(timestamp=[], x=[], y=[])
    # Start actual testing
    time.create_new_value(0.0)
    data.create_new_value(0.0, 0.0)
    data.create_new_value(1.0, 1.0)
    expected_full = __curve_dict(timestamp=[0.0, 1.0], y=[0.0, 1.0])
    expected_new = __curve_dict(timestamp=[0.0, 1.0], x=[0.0, 1.0], y=[0.0, 1.0])
    encx = [0.0]
    ency = [0.0]
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency)
    time.create_new_value(0.5)
    encx = [0.0, 0.5]
    ency = [0.0, 0.5]
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency)
    time.create_new_value(0.75)
    encx = [0.0, 0.75]
    ency = [0.0, 0.75]
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency)
    time.create_new_value(1.0)
    encx = [0.0, 1.0]
    ency = [0.0, 1.0]
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency)
    time.create_new_value(1.5)
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency)
    data.create_new_value(2.0, 0.0)
    encx = [0.0, 1.0, 1.5]
    ency = [0.0, 1.0, 0.5]
    expected_full = __curve_dict(timestamp=[0.0, 1.0, 2.0], y=[0.0, 1.0, 0.0])
    expected_new = __curve_dict(timestamp=[0.0, 1.0, 2.0], x=[0.0, 1.0, 2.0], y=[0.0, 1.0, 0.0])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency)
    time.create_new_value(2.0)
    eocx = [0.0, 1.0, 2.0]
    eocy = [0.0, 1.0, 0.0]
    encx = [0.0]
    ency = [0.0]
    expected_full = __curve_dict(timestamp=[0.0, 1.0, 2.0], y=[0.0, 1.0, 0.0])
    expected_old = __curve_dict(timestamp=[0.0, 1.0, 2.0], x=[0.0, 1.0, 2.0], y=[0.0, 1.0, 0.0])
    expected_new = __curve_dict(timestamp=[2.0], x=[0.0], y=[0.0])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)


def test_clipping_2(qtbot):
    window = __prepare_sliding_pointer_plot_test_window(qtbot, 2)
    plot_item = window.plot.plotItem
    time = window.time_source_mock
    data = window.data_source_mock
    time.create_new_value(0.0)
    data.create_new_value(0.0, 1.0)
    time.create_new_value(1.0)
    data.create_new_value(1.0, 0.0)
    time.create_new_value(1.5)
    data.create_new_value(1.5, 1.5)
    time.create_new_value(2.5)
    data.create_new_value(2.5, 4.5)
    # Starts at x=0.5! Old curve is already partially overdrawn
    eocx = [0.5, 1.0, 1.5, 2.0]
    eocy = [0.5, 0.0, 1.5, 3.0]
    encx = [0.0, 0.5]
    ency = [3.0, 4.5]
    expected_full = __curve_dict(timestamp=[0.0, 1.0, 1.5, 2.5], y=[1.0, 0.0, 1.5, 4.5])
    expected_old = __curve_dict(timestamp=[0.0, 1.0, 1.5, 2.0], x=[0.0, 1.0, 1.5, 2.0], y=[1.0, 0.0, 1.5, 3.0])
    expected_new = __curve_dict(timestamp=[2.0, 2.5], x=[0.0, 0.5], y=[3.0, 4.5])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)


def test_clipping_3(qtbot):
    """
    Test connection between old and new curve
    1. Points in front and after cycle end
    2. Last point in curve is exactly on the cycle end
    """
    window = __prepare_sliding_pointer_plot_test_window(qtbot, 2)
    plot_item = window.plot.plotItem
    time = window.time_source_mock
    data = window.data_source_mock
    time.create_new_value(0.0)
    data.create_new_value(0.0, 0.0)
    time.create_new_value(1.0)
    data.create_new_value(0.75, 0.75)
    time.create_new_value(2.0)
    data.create_new_value(1.75, 1.75)
    time.create_new_value(3.0)
    data.create_new_value(2.75, 2.75)
    eocx = [1.0, 1.75, 2.0]
    eocy = [1.0, 1.75, 2.0]
    encx = [2.0 - 2.0, 2.75 - 2.0]
    ency = [2.0, 2.75]
    expected_full = __curve_dict(timestamp=[0.0, 0.75, 1.75, 2.75], y=[0.0, 0.75, 1.75, 2.75])
    expected_old = __curve_dict(timestamp=[0.0, 0.75, 1.75, 2.0], x=[0.0, 0.75, 1.75, 2.0], y=[0.0, 0.75, 1.75, 2.0])
    expected_new = __curve_dict(timestamp=[2.0, 2.75], x=[0.0, 0.75], y=[2.0, 2.75])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    time.create_new_value(4.0)
    data.create_new_value(3.75, 3.75)
    data.create_new_value(4.0, 4.0)
    time.create_new_value(5.0)
    data.create_new_value(4.75, 4.75)
    eocx = [5.0 - 4.0, 3.75 - 2.0, 4.0 - 2.0]
    eocy = [1 + 2.0, 3.75, 4.0]
    encx = [4.0 - 4.0, 4.75 - 4.0]
    ency = [4.0, 4.75]
    expected_full = __curve_dict(timestamp=[0.0, 0.75, 1.75, 2.75, 3.75, 4.0, 4.75],
                                 y=[0.0, 0.75, 1.75, 2.75, 3.75, 4.0, 4.75])
    expected_old = __curve_dict(timestamp=[2.75, 3.75, 4.0],
                                x=[2.75 - 2.0, 3.75 - 2.0, 4.0 - 2.0],
                                y=[2.75, 3.75, 4.0])
    expected_new = __curve_dict(timestamp=[4.0, 4.75], x=[0.0, 0.75], y=[4.0, 4.75])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)


def test_clipping_4(qtbot):
    """
    Test if the old curve is overdrawn properly as time progresses.
    """
    window = __prepare_sliding_pointer_plot_test_window(qtbot, 2)
    plot_item = window.plot.plotItem
    time = window.time_source_mock
    data = window.data_source_mock
    time.create_new_value(0.0)
    data.create_new_value(0.0, 0.0)
    time.create_new_value(1.0)
    data.create_new_value(1.0, 1.0)
    time.create_new_value(2.0)
    data.create_new_value(2.0, 2.0)
    # Overdrawing
    eocx = [0.0, 1.0, 2.0]
    eocy = [0.0, 1.0, 2.0]
    encx = [0.0]
    ency = [2.0]
    expected_full = __curve_dict(timestamp=[0.0, 1.0, 2.0], y=[0.0, 1.0, 2.0])
    expected_old = __curve_dict(timestamp=[0.0, 1.0, 2.0], x=[0.0, 1.0, 2.0], y=[0.0, 1.0, 2.0])
    expected_new = __curve_dict(timestamp=[2.0], x=[0.0], y=[2.0])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    time.create_new_value(2.5)
    eocx = [0.5, 1.0, 2.0]
    eocy = [0.5, 1.0, 2.0]
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    time.create_new_value(3.0)
    eocx = [1.0, 2.0]
    eocy = [1.0, 2.0]
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    data.create_new_value(2.5, 2.5)
    eocx = [1.0, 2.0]
    eocy = [1.0, 2.0]
    encx = [0.0, 0.5]
    ency = [2.0, 2.5]
    expected_full = __curve_dict(timestamp=[0.0, 1.0, 2.0, 2.5], y=[0.0, 1.0, 2.0, 2.5])
    expected_old = __curve_dict(timestamp=[0.0, 1.0, 2.0], x=[0.0, 1.0, 2.0], y=[0.0, 1.0, 2.0])
    expected_new = __curve_dict(timestamp=[2.0, 2.5], x=[0.0, 0.5], y=[2.0, 2.5])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    data.create_new_value(3.5, 3.5)
    eocx = [1.0, 2.0]
    eocy = [1.0, 2.0]
    encx = [0.0, 0.5, 1.0]
    ency = [2.0, 2.5, 3.0]
    expected_full = __curve_dict(timestamp=[0.0, 1.0, 2.0, 2.5, 3.5], y=[0.0, 1.0, 2.0, 2.5, 3.5])
    expected_old = __curve_dict(timestamp=[0.0, 1.0, 2.0], x=[0.0, 1.0, 2.0], y=[0.0, 1.0, 2.0])
    expected_new = __curve_dict(timestamp=[2.0, 2.5, 3.5], x=[0.0, 0.5, 1.5], y=[2.0, 2.5, 3.5])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    time.create_new_value(3.0)
    eocx = [1.0, 2.0]
    eocy = [1.0, 2.0]
    encx = [0.0, 0.5, 1.0]
    ency = [2.0, 2.5, 3.0]
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    time.create_new_value(3.5)
    eocx = [1.5, 2.0]
    eocy = [1.5, 2.0]
    encx = [0.0, 0.5, 1.5]
    ency = [2.0, 2.5, 3.5]
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    data.create_new_value(4.0, 4.0)
    expected_full = __curve_dict(timestamp=[0.0, 1.0, 2.0, 2.5, 3.5, 4.0], y=[0.0, 1.0, 2.0, 2.5, 3.5, 4.0])
    expected_old = __curve_dict(timestamp=[0.0, 1.0, 2.0], x=[0.0, 1.0, 2.0], y=[0.0, 1.0, 2.0])
    expected_new = __curve_dict(timestamp=[2.0, 2.5, 3.5, 4.0], x=[0.0, 0.5, 1.5, 2.0], y=[2.0, 2.5, 3.5, 4.0])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    time.create_new_value(4.0)
    eocx = [0.0, 0.5, 1.5, 2.0]
    eocy = [2.0, 2.5, 3.5, 4.0]
    encx = [0.0]
    ency = [4.0]
    expected_full = __curve_dict(timestamp=[0.0, 1.0, 2.0, 2.5, 3.5, 4.0], y=[0.0, 1.0, 2.0, 2.5, 3.5, 4.0])
    expected_old = __curve_dict(timestamp=[2.0, 2.5, 3.5, 4.0], x=[0.0, 0.5, 1.5, 2.0], y=[2.0, 2.5, 3.5, 4.0])
    expected_new = __curve_dict(timestamp=[4.0], x=[0.0], y=[4.0])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)


def test_time_and_data_update_order(qtbot):
    """
    Compare the outcome of timing-update after data-update to the opposite order.
    Both should lead to the same result if the sent timestamps and data is the same.
    """
    window_1 = __prepare_sliding_pointer_plot_test_window(qtbot, 2)
    window_2 = __prepare_sliding_pointer_plot_test_window(qtbot, 2)
    plot_item_1 = window_1.plot.plotItem
    plot_item_2 = window_2.plot.plotItem
    time_1 = window_1.time_source_mock
    time_2 = window_2.time_source_mock
    data_1 = window_1.data_source_mock
    data_2 = window_2.data_source_mock
    time_1.create_new_value(0.0)
    data_1.create_new_value(0.0, 0.0)
    data_2.create_new_value(0.0, 0.0)
    time_2.create_new_value(0.0)
    d_2 = plot_item_2.get_data_items_data()
    assert __check_curves(plot_item_1, plot_item_2.full_curve_buffer, plot_item_2.full_curve_old,
                          plot_item_2.full_curve_new)
    assert __check_plot_data_items_data(plot_item_1, d_2['new_curve_x'], d_2['new_curve_y'],
                                        d_2['old_curve_x'], d_2['old_curve_x'])
    time_1.create_new_value(1.0)
    data_1.create_new_value(0.5, 0.5)
    data_2.create_new_value(0.5, 0.5)
    time_2.create_new_value(1.0)
    assert __check_curves(plot_item_1, plot_item_2.full_curve_buffer, plot_item_2.full_curve_old,
                          plot_item_2.full_curve_new)
    assert __check_plot_data_items_data(plot_item_1, d_2['new_curve_x'], d_2['new_curve_y'],
                                        d_2['old_curve_x'], d_2['old_curve_x'])
    time_1.create_new_value(2.0)
    data_1.create_new_value(1.0, 1.0)
    data_1.create_new_value(1.5, 1.5)
    data_1.create_new_value(2.0, 2.0)
    data_2.create_new_value(1.0, 1.0)
    data_2.create_new_value(1.5, 1.5)
    data_2.create_new_value(2.0, 2.0)
    time_2.create_new_value(2.0)
    assert __check_curves(plot_item_1, plot_item_2.full_curve_buffer, plot_item_2.full_curve_old,
                          plot_item_2.full_curve_new)
    assert __check_plot_data_items_data(plot_item_1, d_2['new_curve_x'], d_2['new_curve_y'],
                                        d_2['old_curve_x'], d_2['old_curve_x'])


def test_data_delivered_in_wrong_order(qtbot):
    """
    Test if a sent dataset with an timestamp older than an already
    added one will be inserted at the right index. It is expected,
    that they are inserted in the way, that the internal saved data
    is always ordered by the timestamps of the datasets.
    """
    window = __prepare_sliding_pointer_plot_test_window(qtbot, 2)
    plot_item = window.plot.plotItem
    time = window.time_source_mock
    data = window.data_source_mock
    time.create_new_value(0.0)
    data.create_new_value(1.0, 1.0)
    time.create_new_value(0.5)
    data.create_new_value(0.5, 0.5)
    time.create_new_value(1.0)
    eocx = []
    eocy = []
    encx = [0.5, 1.0]
    ency = [0.5, 1.0]
    expected_full = __curve_dict(timestamp=[0.5, 1.0], y=[0.5, 1.0])
    expected_old = __curve_dict(timestamp=[], x=[], y=[])
    expected_new = __curve_dict(timestamp=[0.5, 1.0], x=[0.5, 1.0], y=[0.5, 1.0])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    data.create_new_value(1.75, 1.75)
    data.create_new_value(1.5, 1.5)
    data.create_new_value(1.25, 1.25)
    time.create_new_value(1.9)
    eocx = []
    eocy = []
    encx = [0.5, 1.0, 1.25, 1.5, 1.75]
    ency = [0.5, 1.0, 1.25, 1.5, 1.75]
    expected_full = __curve_dict(timestamp=[0.5, 1.0, 1.25, 1.5, 1.75], y=[0.5, 1.0, 1.25, 1.5, 1.75])
    expected_old = __curve_dict(timestamp=[], x=[], y=[])
    expected_new = __curve_dict(timestamp=[0.5, 1.0, 1.25, 1.5, 1.75], x=[0.5, 1.0, 1.25, 1.5, 1.75],
                                y=[0.5, 1.0, 1.25, 1.5, 1.75])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    data.create_new_value(2.1, 2.1)
    time.create_new_value(2.1)
    data.create_new_value(1.9, 1.9)  # Update with plot that belongs now to old curve
    eocx = [0.5, 1.0, 1.25, 1.5, 1.75, 1.9, 2.0]
    eocy = [0.5, 1.0, 1.25, 1.5, 1.75, 1.9, 2.0]
    encx = [2.0 - 2.0, 2.1 - 2.0]
    ency = [2.0, 2.1]
    expected_full = __curve_dict(timestamp=[0.5, 1.0, 1.25, 1.5, 1.75, 1.9, 2.1],
                                 y=[0.5, 1.0, 1.25, 1.5, 1.75, 1.9, 2.1])
    expected_old = __curve_dict(timestamp=[0.5, 1.0, 1.25, 1.5, 1.75, 1.9, 2.0],
                                x=[0.5, 1.0, 1.25, 1.5, 1.75, 1.9, 2.0], y=[0.5, 1.0, 1.25, 1.5, 1.75, 1.9, 2.0])
    expected_new = __curve_dict(timestamp=[2.0, 2.1], x=[0.0, 2.1 - 2.0], y=[2.0, 2.1])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)


def test_timestamp_delivered_in_wrong_order(qtbot):
    """
    Test the handling of timestamps that are older than an already
    received one. It is to expected that the timing update is ignored.
    """
    window = __prepare_sliding_pointer_plot_test_window(qtbot, 2)
    plot_item = window.plot.plotItem
    time = window.time_source_mock
    data = window.data_source_mock
    time.create_new_value(0.0)
    data.create_new_value(0.5, 0.5)
    time.create_new_value(1.0)
    data.create_new_value(1.0, 1.0)
    eocx = []
    eocy = []
    encx = [0.5, 1.0]
    ency = [0.5, 1.0]
    expected_full = __curve_dict(timestamp=[0.5, 1.0], y=[0.5, 1.0])
    expected_old = __curve_dict(timestamp=[], x=[], y=[])
    expected_new = __curve_dict(timestamp=[0.5, 1.0], x=[0.5, 1.0], y=[0.5, 1.0])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    # Timing update should not have an impact
    time.create_new_value(0.5)
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    data.create_new_value(2.1, 2.1)
    time.create_new_value(2.1)
    eocx = [0.5, 1.0, 2.0]
    eocy = [0.5, 1.0, 2.0]
    encx = [2.0 - 2.0, 2.1 - 2.0]
    ency = [2.0, 2.1]
    expected_full = __curve_dict(timestamp=[0.5, 1.0, 2.1], y=[0.5, 1.0, 2.1])
    expected_old = __curve_dict(timestamp=[0.5, 1.0, 2.0], x=[0.5, 1.0, 2.0], y=[0.5, 1.0, 2.0])
    expected_new = __curve_dict(timestamp=[2.0, 2.1], x=[2.0 - 2.0, 2.1 - 2.0], y=[2.0, 2.1])
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    # Simulate going back to the old curve
    time.create_new_value(1.9)
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)
    # Delivering the same timestamp 2x should have no impact as well
    time.create_new_value(2.1)
    assert __check_curves(plot_item, expected_full, expected_old, expected_new)
    assert __check_plot_data_items_data(plot_item, encx, ency, eocx, eocy)


# ~~~~~~~~~~~~~~ Helper Functions ~~~~~~~~~~~~~~+


def __prepare_sliding_pointer_plot_test_window(qtbot, cycle_size):
    plot_config = {
        'cycle_size': cycle_size,
        'plotting_style': Pws.SLIDING_POINTER,
        'time_progress_line': True,
        'v_draw_line': False,
        'h_draw_line': False,
        'draw_point': True
    }
    window = ExtendedPlotWidgetTestingWindow(plot_config)
    window.show()
    qtbot.addWidget(window)
    return window


def __check_curves(plot_item: Sppi, expected_full: Dict[str, List[float]], expected_old: Dict[str, List[float]],
                   expected_new: Dict[str, List[float]]):
    result = True
    result &= json.dumps(plot_item.full_curve_buffer) == json.dumps(expected_full)
    result &= json.dumps(plot_item.full_curve_new) == json.dumps(expected_new)
    result &= json.dumps(plot_item.full_curve_old) == json.dumps(expected_old)
    return result


def __check_plot_data_items_data(plot_item: Sppi, expected_nc_x: List[float] = [], expected_nc_y: List[float] = [],
                                 expected_oc_x: List[float] = [], expected_oc_y: List[float] = []):
    result = True
    data = plot_item.get_data_items_data()
    result &= data['new_curve_x'] == expected_nc_x
    result &= data['new_curve_y'] == expected_nc_y
    result &= data['old_curve_x'] == expected_oc_x
    result &= data['old_curve_y'] == expected_oc_y
    return result


def __curve_dict(timestamp: List[float] = None, x: List[float] = None, y: List[float] = None):
    result = {}
    if timestamp is not None:
        result['timestamps'] = timestamp
    if x is not None:
        result['x'] = x
    if y is not None:
        result['y'] = y
    return result


def __simple_linear_update(time_source: TimingSourceMock, data_source: DataSourceMock, timestamps: List[float],
                           current_dataset_timestamp: float, datasets_delta: float):
    for index, timestamp in enumerate(iterable=timestamps):
        time_source.create_new_value(timestamp)
        while current_dataset_timestamp <= timestamp:
            data_source.create_new_value(current_dataset_timestamp, current_dataset_timestamp)
            current_dataset_timestamp += datasets_delta

