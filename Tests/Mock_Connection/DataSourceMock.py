from Connection.interface.UpdateSource import UpdateSource


class DataSourceMock(UpdateSource):

    """
    Class for sending the right signals to the ExtendedPlotWidget.
    This allows precise control over updates that are sent to the widget
    compared to timer based solutions.
    """

    def __init__(self, *args, **kwargs):
        super(DataSourceMock, self).__init__(*args, **kwargs)

    def create_new_value(self, timestamp: float, value: float) -> None:
        new_data = {
            'x': timestamp,
            'y': value
        }
        self.DATA_SIGNAL.emit(new_data)
