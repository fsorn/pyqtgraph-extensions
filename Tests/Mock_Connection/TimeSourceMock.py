from Connection.interface.UpdateSource import UpdateSource


class TimingSourceMock(UpdateSource):

    """
    Class for sending the right signals to the ExtendedPlotWidget.
    This allows precise control over updates that are sent to the widget
    compared to timer based solutions.
    """

    def __init__(self, *args, **kwargs):
        super(TimingSourceMock, self).__init__(*args, **kwargs)

    def create_new_value(self, timestamp: float) -> None:
        self.TIME_SIGNAL.emit(timestamp)
