# PyQtGraph-Extensions

Extensions for PyQtGraph that allow some custom plotting options

## Components of this Project
This project contains mainly widget related classes as well as classes for
connecting these widgets to data sources and a timing system for time based
plotting.

## Usage
### Requirements
For using the library you need to have PyQt5 and pyqtgraph installed.
### Example Usage
An example usage has been added in main.py

![Sliding Pointer Widget](Documentation/SlidingPointerWidget.png?raw=true "Sliding Pointer Widget")
![Scrolling Plot Widget](Documentation/ScrollingPlotWidget.png?raw=true "Scrolling Plot Widget")
