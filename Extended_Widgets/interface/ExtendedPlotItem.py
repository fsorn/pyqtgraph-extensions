from Connection.interface.UpdateSource import UpdateSource
from pyqtgraph import PlotItem, PlotDataItem, mkPen
from datetime import datetime


class ExtendedPlotItem(PlotItem):

    """
    Definition of a common interface for all extended PlotItems.
    Common functionality can be defined here and implemented in
    the subclasses. Shared functions can be handled here.
    """

    def __init__(self, timing_source: UpdateSource, data_source: UpdateSource,
                 time_progress_line: bool = True, v_draw_line: bool = False,
                 h_draw_line: bool = False, draw_point: bool = False, **kwargs):
        """

        :param time_progress_line: Draw a line that represents the current time
        :param v_draw_line: Draw a vertical line at the position of the newest point
        :param h_draw_line: Draw a vertical line at the position of the newest point
        :param draw_point: Represent the newest point drawn with a bigger point
        :param kwargs: Keyword Arguments that will be passed to PlotItem
        """
        super(ExtendedPlotItem, self).__init__(**kwargs)
        self.time_progress_line = time_progress_line
        self.v_draw_line = v_draw_line
        self.h_draw_line = h_draw_line
        self.draw_point = draw_point
        self.last_timestamp: float = -1
        self.time_line = None
        self.plotting_line_v = None
        self.plotting_line_h = None
        self.plotting_line_point = None
        # Get timing updates from the Timing System, do not use local time for any time based actions!
        timing_source.TIME_SIGNAL.connect(self._handle_timing_update)
        data_source.DATA_SIGNAL.connect(lambda dct: self.plot_append(dct['x'], dct['y']))

    # Abstract
    def plot_append(self, x, y, plot_data_item: PlotDataItem = None) -> None:
        """
        Append a point with given X and Y value to the current graph. Provide a reference to the
        fitting PlotDataItem if there exists more than one.\n
        :param x: X position of the point
        :param y: Y position of the point
        :param plot_data_item: Reference for the PlotDataItem the point should be appended to
        """
        pass

    def get_data_items_filtered(self):
        """
        Get only data-items that are displaying actual data. This excludes for example the point
        that represents the current write index.\n
        :return: filtered data-items that are displaying curves.
        """
        items = self.dataItems
        items.remove(self.plotting_line_point)

    def _draw_time_line_decorator(self, timestamp: float, position: float):
        """
        Redraw the timing line according to a passed timestamp. Alternatively the line can also
        be drawn at a custom position by providing the position parameter, if the position is
        different from the provided timestamp (f.e. through offsets)\n
        :param timestamp: Timestamp that should be displayed at the line
        :param position: Timestamp where the line should be drawn, if None -> position = timestamp
        :return:
        """
        if self.time_line is not None:
            if position is None:
                position = timestamp
            self.time_line.setValue(position)
            self.time_line.label.setText(datetime.fromtimestamp(timestamp).strftime('%H:%M:%S'))

    def _draw_plotting_position_decorators(self, x, y) -> None:
        """
        Draw all decorator at the given position, if they have been created.\n
        :param x: X-Postion to move the decorators to
        :param y: Y-Postion to move the decorators to
        """
        if self.plotting_line_v is not None:
            self.plotting_line_v.setValue(x)
        if self.plotting_line_h is not None:
            self.plotting_line_h.setValue(y)
        if self.plotting_line_point is not None:
            self.plotting_line_point.setData({'x': [x], 'y': [y]})

    # Abstract
    def _handle_timing_update(self, timestamp: float) -> None:
        """
        Handle an update provided by the timing source.
        Implement functionality in the specific subclass.\n
        :param timestamp: Updated timestamp provided by the timing source
        """
        # -1 = Value give in the constructor -> initial time update
        if self.last_timestamp == -1:
            label_opts = {"movable": True, "position": 0.96}
            self.time_line = self.addLine(timestamp, pen=(mkPen(80, 80, 80)),
                                          label=datetime.fromtimestamp(timestamp).strftime('%H:%M:%S'),
                                          labelOpts=label_opts) if self.time_progress_line else None
            self.plotting_line_v = self.addLine(x=timestamp) if self.v_draw_line else None
            self.plotting_line_h = self.addLine(y=0) if self.h_draw_line else None
            self.plotting_line_point = self.plot([timestamp], [0], symbol='o') if self.draw_point else None
        self.last_timestamp = timestamp

