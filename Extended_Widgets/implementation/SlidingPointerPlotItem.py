from Extended_Widgets.interface.ExtendedPlotItem import ExtendedPlotItem
from Extended_Widgets.implementation.ExtendedAxisItems import RelativeTimeAxisItem
from Extended_Widgets.implementation.PlotItemUtils import PlotItemUtils
from pyqtgraph import PlotDataItem, mkPen
from typing import List, Dict
import logging

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)

# Keep maximum of 1 Mio points in the internal buffer, if reached throw first half away
MAX_BUFFER_SIZE = 1000000


class SlidingPointerPlotItem(ExtendedPlotItem):

    """
    Displays data as a sliding pointer widget similar to a heart rate monitor.
    The graph itself stays fixed in position and has a fixed length that it does
    not exceed. As soon as the drawing reaches the end, the graph gets redrawn
    beginning at the start. The old curve gets incrementally overwritten by the
    new values.
    The x-values of all lines in the graph will be shifted backwards according to
    the cycle length (like x % cycle_length) so the area with the visual change does
    not move.
    """

    def __init__(self, cycle_size: int = 100, **kwargs):
        """
        Create a new SlidingPointerPlotItem.\n
        :param cycle_size: The cycle length in ms.
        :param kwargs: Keyword Arguments delegated to Superclass
        """
        super(SlidingPointerPlotItem, self).__init__(**kwargs)
        # x values not needed here
        self.full_curve_buffer: Dict[str, List[float]] = {
            'timestamps': [],
            'y': []
        }
        self.full_curve_old: Dict[str, List[float]] = {
            'timestamps': [],
            'x': [],
            'y': []
        }
        self.old_curve_data_item: PlotDataItem = self.plot(pen=mkPen(80, 80, 80))
        self.full_curve_new: Dict[str, List[float]] = {
            'timestamps': [],
            'x': [],
            'y': []
        }
        # Save data passed to PlotDataItems for testing purposes
        self.__data_items_data: Dict[str, List[float]] = {
            'old_curve_x': [],
            'old_curve_y': [],
            'new_curve_x': [],
            'new_curve_y': [],
        }
        self.new_curve_data_item: PlotDataItem = self.plot(pen=mkPen(255, 255, 255))
        # Get Cycle start, end etc. on first time update
        self.__first_time_update = True
        self.cycle_number: int = 0
        self.cycle_size: int = cycle_size

    # Handle Data Source Update

    def plot_append(self, x, y) -> None:
        """
        A new plot is always appended to the full curve buffer dict, not to the new
        and old curve dicts. They are subsets of the full curve buffer and will be
        modified by updates from the Timing Source according to the current time.\n
        If the new point appended is not in the right order (point has lower time-stamp
        than one that is already in the full curve buffer), they will be added at
        the right position according to their timestamp. This forces the full curve buffer
        to always stay ordered no matter what order points are appended in. \n
        :param x: timestamp of the new point
        :param y: value of the new point
        """
        logger.debug("Number Points in new Curve X : " + str(len(self.full_curve_new['x'])))
        logger.debug("Number Points in new Curve Y : " + str(len(self.full_curve_new['y'])))
        logger.debug("Cycle Number:                  " + str(self.cycle_number))
        if len(self.full_curve_buffer['timestamps']) < 1 or x > self.full_curve_buffer['timestamps'][-1]:
            self.full_curve_buffer['timestamps'].append(x)
            self.full_curve_buffer['y'].append(y)
        else:
            index = -1
            stop = -1 * len(self.full_curve_buffer['timestamps'])
            while x < self.full_curve_buffer['timestamps'][index] and index > stop:
                index -= 1
            self.full_curve_buffer['timestamps'].insert(index + 1, x)
            self.full_curve_buffer['y'].insert(index + 1, y)
        # Update with current timestamp but with new data
        if self.last_timestamp != -1:
            self._handle_timing_update(self.last_timestamp)

    # Handle Timing Source Update

    def _handle_timing_update(self, timestamp: float) -> None:
        """
        Handle a update in the current time triggered by the timing source
        With the new timestamp the data subset for the new and old curve are updated
        clipped and drawn.
        If the timestamp is older than the current known one, it will be ignored and
        interpreted as delivered as too late
        """
        if timestamp >= self.last_timestamp:
            super(SlidingPointerPlotItem, self)._handle_timing_update(timestamp)
            self.__handle_initial_time_update()
            old_cycle_number = self.cycle_number
            self.cycle_number = (timestamp - self.cycle_start) // self.cycle_size
            if old_cycle_number < self.cycle_number:
                self.__handle_new_cycle()
            # Points located at cycle start would otherwise also be added to cycle number "-1"
            if self.cycle_number > 0:
                self.__update_old_curve_data()
            self.__update_new_curve_data()
            self.__connect_old_to_new_curve()
            self.__update_new_curve_data_item()
            self.__update_old_curve_data_item()
            position = self.cycle_start + ((timestamp - self.cycle_start) % self.cycle_size)
            self._draw_time_line_decorator(timestamp=timestamp, position=position)
            x = self.full_curve_new['x'][-1] if len(self.full_curve_new['x']) > 0 else self.cycle_start
            y = self.full_curve_new['y'][-1] if len(self.full_curve_new['y']) > 0 else 0
            self._draw_plotting_position_decorators(x, y)

    # New and Old Curve Subsample Functions

    def __handle_initial_time_update(self):
        if self.__first_time_update:
            self.cycle_start: float = self.__get_current_time_line_x_pos()
            self.cycle_end: float = self.cycle_start + self.cycle_size * (self.cycle_number + 1)
            # boundaries at cycle start and end
            self.addLine(x=self.cycle_start, pen=mkPen(128, 128, 128))
            self.addLine(x=self.cycle_end, pen=mkPen(128, 128, 128))
            self.__first_time_update = False
            axis = self.getAxis('bottom')
            if isinstance(axis, RelativeTimeAxisItem):
                axis.set_start_time(self.cycle_start)

    def __handle_new_cycle(self):
        if len(self.full_curve_buffer['timestamps']) > MAX_BUFFER_SIZE:
            self.full_curve_buffer['timestamps'] = self.full_curve_buffer['timestamps'][MAX_BUFFER_SIZE // 2:]
            self.full_curve_buffer['y'] = self.full_curve_buffer['y'][MAX_BUFFER_SIZE // 2:]

    def __update_old_curve_data(self) -> None:
        """
        Find all points in the full curve buffer that are located
        in the previous cycle
        """
        prev_cycle_start = self.__get_previous_cycle_start_timestamp()
        prev_cycle_end = self.__get_previous_cycle_end_timestamp()
        start = PlotItemUtils.bin_search_surrounding_points(
            self.full_curve_buffer['timestamps'], prev_cycle_start)['after']
        end = PlotItemUtils.bin_search_surrounding_points(
            self.full_curve_buffer['timestamps'], prev_cycle_end)['before']
        if start != -1 and end != -1:
            self.full_curve_old['timestamps'] = self.full_curve_buffer['timestamps'][start:end + 1]
            self.full_curve_old['x'] = [x - self.__get_previous_cycle_offset() for x in
                                        self.full_curve_buffer['timestamps'][start:end + 1]]
            self.full_curve_old['y'] = self.full_curve_buffer['y'][start:end + 1]
        else:
            # No points old enough for an old curve yet
            self.full_curve_old['timestamps'] = []
            self.full_curve_old['x'] = []
            self.full_curve_old['y'] = []

    def __connect_old_to_new_curve(self):
        if len(self.full_curve_old['timestamps']) > 0 and len(self.full_curve_new['timestamps']) > 0:
            point_to_connect_from = {
                'x': self.full_curve_old['timestamps'][-1],
                'y': self.full_curve_old['y'][-1]
            }
            point_to_connect_to = {
                'x': self.full_curve_new['timestamps'][0],
                'y': self.full_curve_new['y'][0]
            }
            cycle_end = self.__get_previous_cycle_end_timestamp()
            point = PlotItemUtils.calc_intersection(point_to_connect_from, point_to_connect_to, cycle_end)
            if point is not None:
                # Don't add duplicates if intersection is already point in curve
                if len(self.full_curve_old['timestamps']) == 0 or point_to_connect_from != point:
                    self.full_curve_old['timestamps'].append(point['x'])
                    self.full_curve_old['x'].append(self.cycle_end)
                    self.full_curve_old['y'].append(point['y'])
                if len(self.full_curve_new['timestamps']) == 0 or point_to_connect_to != point:
                    self.full_curve_new['timestamps'] = [point['x']] + self.full_curve_new['timestamps']
                    self.full_curve_new['x'] = [self.cycle_start] + self.full_curve_new['x']
                    self.full_curve_new['y'] = [point['y']] + self.full_curve_new['y']

    def __update_new_curve_data(self):
        """
        Find all points in the full curve buffer that are located
        in the previous cycle
        """
        cur_cycle_start = self.__get_current_cycle_start_timestamp()
        cur_cycle_end = self.__get_current_cycle_end_timestamp()
        start = PlotItemUtils.bin_search_surrounding_points(
            self.full_curve_buffer['timestamps'], cur_cycle_start)['after']
        end = PlotItemUtils.bin_search_surrounding_points(
            self.full_curve_buffer['timestamps'], cur_cycle_end)['before']
        if start != -1 and end != -1:
            self.full_curve_new['timestamps'] = self.full_curve_buffer['timestamps'][start:end + 1]
            self.full_curve_new['x'] = [x - self.__get_current_cycle_offset() for x in
                                        self.full_curve_buffer['timestamps'][start:end + 1]]
            self.full_curve_new['y'] = self.full_curve_buffer['y'][start:end + 1]
        else:
            # No points old enough for an old curve yet
            self.full_curve_new['timestamps'] = []
            self.full_curve_new['x'] = []
            self.full_curve_new['y'] = []

    def __update_new_curve_data_item(self) -> None:
        """
        Updates the data displayed with the new curves data item.
        A temporary point will be added if the the new point exceeds
        the current time (because of f.e. inaccurate timestamp)
        """
        i = PlotItemUtils.intersect(self.full_curve_new, self.__get_current_time_line_x_pos())
        new_curve_x = self.full_curve_new['x'][:i['last_before_index'] + 1]
        new_curve_y = self.full_curve_new['y'][:i['last_before_index'] + 1]
        if i['intersection'] and i['intersection']['x'] != new_curve_x[-1]\
                and i['intersection']['y'] != new_curve_y[-1]:
            new_curve_x += [i['intersection']['x']]
            new_curve_y += [i['intersection']['y']]
        self.__data_items_data['new_curve_x'] = new_curve_x
        self.__data_items_data['new_curve_y'] = new_curve_y
        self.new_curve_data_item.setData({'x': new_curve_x, 'y': new_curve_y})

    def __update_old_curve_data_item(self) -> None:
        """
        Updates the data displayed with the new curves data item.
        A temporary point will be added if the the new point exceeds
        the current time (because of f.e. inaccurate timestamp)
        """
        i = PlotItemUtils.intersect(self.full_curve_old, self.__get_current_time_line_x_pos())
        if i['intersection'] and i['intersection']['x'] != self.full_curve_old['x'][i['first_after_index']]\
                and i['intersection']['y'] != self.full_curve_old['y'][i['first_after_index']]:
            old_curve_x = [i['intersection']['x']]
            old_curve_y = [i['intersection']['y']]
        else:
            old_curve_x = []
            old_curve_y = []
        old_curve_x += [] if i['first_after_index'] == -1 else self.full_curve_old['x'][i['first_after_index']:]
        old_curve_y += [] if i['first_after_index'] == -1 else self.full_curve_old['y'][i['first_after_index']:]
        self.__data_items_data['old_curve_y'] = old_curve_y
        self.__data_items_data['old_curve_x'] = old_curve_x
        self.old_curve_data_item.setData({'x': old_curve_x, 'y': old_curve_y})

    # Cycle Timestamp Utilities

    def __get_current_cycle_start_timestamp(self):
        return self.cycle_start + self.cycle_number * self.cycle_size

    def __get_previous_cycle_start_timestamp(self):
        return self.cycle_start + (self.cycle_number - 1) * self.cycle_size

    def __get_current_cycle_end_timestamp(self):
        return self.cycle_end + self.cycle_number * self.cycle_size

    def __get_previous_cycle_end_timestamp(self):
        return self.cycle_end + (self.cycle_number - 1) * self.cycle_size

    def __get_current_cycle_offset(self) -> int:
        current_offset = self.cycle_size * self.cycle_number
        return self.cycle_size * self.cycle_number

    def __get_previous_cycle_offset(self) -> int:
        return self.cycle_size * (self.cycle_number - 1)

    def __get_current_time_line_x_pos(self) -> float:
        current_position = self.last_timestamp - self.__get_current_cycle_offset()
        return self.last_timestamp - self.__get_current_cycle_offset()

    # Testing utilities

    def get_data_items_data(self) -> Dict[str, List[float]]:
        return self.__data_items_data
