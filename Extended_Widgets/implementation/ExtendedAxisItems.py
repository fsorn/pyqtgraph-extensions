from pyqtgraph import AxisItem
from datetime import datetime

class TimeAxisItem(AxisItem):

    """
    Axis Item that shows timestamps as strings in format HH:MM:SS
    """

    def __init__(self, *args, **kwargs):
        super(TimeAxisItem, self).__init__(*args, **kwargs)

    def tickStrings(self, values, scale, spacing):
        try:
            return [datetime.fromtimestamp(value).strftime('%H:%M:%S') for value in values]
        except ValueError:
            return [""]


class RelativeTimeAxisItem(AxisItem):

    """
    Axis Item that displays timestamps as difference in seconds to an given start time.
    Example:    start-time 01:00:00 and tick is 01:00:10 -> '+10s' will be displayed
                at the position of the tick
    """

    def __init__(self, *args, **kwargs):
        super(RelativeTimeAxisItem, self).__init__(*args, **kwargs)
        self.start = 0

    def set_start_time(self, timestamp):
        self.start = timestamp
    
    def tickStrings(self, values, scale, spacing):
        arred = lambda x, n: x * (10 ** n) // 1 / (10 ** n)  # round to n decimals
        try:
            return [
                ("+" if (value - self.start) > 0 else "") + str(arred(value - self.start, 2)) + "s" for value in values]
        except ValueError:
            return [""]
