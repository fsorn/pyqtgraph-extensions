from typing import List, Dict
from datetime import datetime
import logging

logging.basicConfig(level=logging.ERROR)
logger = logging.getLogger(__name__)

class PlotItemUtils:

    """
    Collection of static Utility Functions to decrease complexity
    in ExtendedPlotItem Classes
    """

    @staticmethod
    def intersect(graph_points: Dict[str, List[float]], vertical_line_x_position: float):
        """
        Intersect a plot line with a vertical line, for example a boundary defined by a x-value.
        First we seach for the
        :param graph_points: Dict that contains the points that the graph is built from {'x':...,'y':...}
        :param vertical_line_x_position: x position of a vertical line the intersection should be calculated with
        :return: point indices before and after the lien and the point of intersection, if it exists
        """
        result = {
            'last_before_index': -1,
            'first_after_index': -1,
            'intersection': None
        }
        x_positions = graph_points['x']
        y_positions = graph_points['y']
        if not len(x_positions) == len(y_positions):
            logger.warning(
                "Lenght:(" + str(len(x_positions)) + ", " + str(len(y_positions)) +
                "), Passed Points: " + str(graph_points))
            raise ValueError("The count of X indices and Y values is not the same.")
        surrounding_points = PlotItemUtils.bin_search_surrounding_points(x_positions, vertical_line_x_position)
        result['last_before_index'] = surrounding_points['before']
        result['first_after_index'] = surrounding_points['after']
        # Line is actually in between two points -> Calculate intersection point
        if result['last_before_index'] != -1 and result['first_after_index'] != -1:
            last_before_obj = {
                'x': x_positions[surrounding_points['before']],
                'y': y_positions[surrounding_points['before']]
            }
            first_after_obj = {
                'x': x_positions[surrounding_points['after']],
                'y': y_positions[surrounding_points['after']]
            }
            # Intersection between Old Curve
            result['intersection'] = PlotItemUtils.calc_intersection(
                last_before_obj, first_after_obj, vertical_line_x_position
            )
        return result

    @staticmethod
    def calc_intersection(point_1: dict, point_2: dict, new_point_x_position: float) -> dict:
        """
        Calculates the position of a point with a given X value that is located on the
        straight line between point_1 and point_2. \n
        :param point_1: Point with smaller X value ({'x':..., 'y':...})
        :param point_2: Points with bigger Y value ({'x':..., 'y':...})
        :param new_point_x_position: X position where point should be calculated
        :return: Point between p1 and p2 plotted at the passed position
        """
        k1 = point_1.keys()
        k2 = point_2.keys()
        # Handle Data in the wrong format
        if not('x' in k1 and 'x' in k2 and 'y' in k1 and 'y' in k2):
            logger.warning("Parameters are in wrong format.")
            logger.warning("Point 1:    " + str(point_1))
            logger.warning("Point 2:    " + str(point_2))
            logger.warning("X Position: " + str(new_point_x_position))
            raise TypeError("The passed points do not have the right form.")
        # Handle special cases of intersections
        if point_2['x'] < point_1['x']:
            logger.warning("Parameters are in wrong order. This might hint, that a bug appeared in the code before.")
            logger.warning("Point 1:    " + str(point_1))
            logger.warning("Point 2:    " + str(point_2))
            logger.warning("X Position: " + str(new_point_x_position))
            # Change order of points
            tmp = point_1
            point_1 = point_2
            point_2 = tmp
        if new_point_x_position > point_2['x'] or new_point_x_position < point_1['x']:
            logger.warning("New position not between the passed points, listing their X positions: New=" +
                           str(new_point_x_position) + ", P1=" + str(point_1['x']) + ", P2=" + str(point_2['x']))
            return None
        if point_2['x'] == point_1['x']:
            return {'x': point_1['x'], 'y': point_1['y']}
        # Calculate intersection with boundary
        delta_p1_p2_x = point_2['x'] - point_1['x']
        delta_p1_p2_y = point_2['y'] - point_1['y']
        delta_p1_line_x = new_point_x_position - point_1['x']
        temp_x = point_1['x'] + delta_p1_p2_x * (delta_p1_line_x / delta_p1_p2_x)
        temp_y = point_1['y'] + delta_p1_p2_y * (delta_p1_line_x / delta_p1_p2_x)
        return {'x': temp_x, 'y': temp_y}

    @staticmethod
    def bin_search_surrounding_points(x: List[int], l_x: int):
        """
        Searches for the two points closest to the given position of the vertical line.
        Multiple cases can happen here:\n
        1: Line between two points -> return two points \n
        2: Line outside the point range -> return closest single point + (-1) on the one that does not exist \n
        3: Less then two points -> one point 0, no points -1 for both \n
        :param x:      List of x coordinates
        :param l_x:    x coordinate of line
        :return:       {'before': ..., 'after': ...} -> Values are indices of the found points in the passed array
        """
        r = {
            'before': 0,
            'after': len(x) - 1
        }
        if len(x) < 1:
            r['after'] = -1
            r['before'] = -1
        # Line in front of complete X range
        elif l_x < x[r['before']]:
            r['after'] = r['before']
            r['before'] = -1
        # Line after complete X range
        elif l_x > x[r['after']]:
            r['after'] = -1
            r['before'] = len(x) - 1
        # Line in X Range
        else:
            found = False
            while r['before'] <= r['after'] and not found:
                midpoint = (r['before'] + r['after']) // 2
                # Are points surrounding the line already?
                if len(x) == 0:
                    found = True
                elif len(x) == 1:
                    found = True
                    if x[0] < l_x:
                        r['before'] = 0
                        r['after'] = -1
                    elif x[0] > l_x:
                        r['before'] = -1
                        r['after'] = 0
                    elif x[0] == 0:
                        r['after'] = 0
                        r['before'] = 0
                elif len(x) > 1 and x[r['before']] <= l_x <= x[r['before'] + 1]:
                    found = True
                    if x[midpoint] == l_x:
                        r['after'] = midpoint
                        r['before'] = midpoint
                    elif x[r['before']] == l_x:
                        r['after'] = r['before']
                    elif x[r['before'] + 1] == l_x:
                        r['before'] = r['before'] + 1
                        r['after'] = r['before']
                    else:
                        r['after'] = r['before'] + 1
                else:
                    if l_x < x[midpoint]:
                        r['after'] = midpoint
                    elif l_x > x[midpoint]:
                        r['before'] = midpoint
                    else:
                        # Line position exactly on a point
                        found = True
                        r['after'] = midpoint
                        r['before'] = midpoint
        return r
