from pyqtgraph import PlotWidget
from Extended_Widgets.implementation.ExtendedAxisItems import TimeAxisItem, RelativeTimeAxisItem
from Extended_Widgets.implementation.SlidingPointerPlotItem import SlidingPointerPlotItem
from Extended_Widgets.implementation.ScrollingPlotItem import ScrollingPlotItem
from Connection.interface.UpdateSource import UpdateSource
from enum import Enum


class PlotWidgetStyle(Enum):

    """ PlotWidgetStyle Enumeration\n
    Enumeration for the different available styles for the Extended_Widgets\n
        SCROLLING_PLOT: New data gets appended and old one clipped before a specific time point. This
        creates a scrolling movement of the graph in positive x direction\n
        SLIDING_POINTER: A moving line redraws periodically an non moving line graph. The old version
        gets overdrawn as soon as a new point exists that is plotted to the same position in x range
    """
    SCROLLING_PLOT = 1
    SLIDING_POINTER = 2


# Prepares data, so the PlotWidget can show it
# Plotting is done by inner PlotWidget, style of displaying
# is achieved by manipulating the data given as input
class ExtendedPlotWidget(PlotWidget):

    """
    Extended version of PyQtGraphs PlotWidget with additional functionality
    and plotting styles and high level functions for data and timing updates
    provided by fitting Signals
    """

    def __init__(self, timing_source: UpdateSource, data_source: UpdateSource, config: dict, *args, **kwargs):
        super(ExtendedPlotWidget, self).__init__(*args, **kwargs)
        self.timing_source = timing_source
        self.data_source = data_source
        self.cycle_size = config.get('cycle_size', 100)
        self.plotting_style = config.get('plotting_style', PlotWidgetStyle.SLIDING_POINTER)
        self.time_progress_line = config.get('time_progress_line', False)
        self.v_draw_line = config.get('v_draw_line', False)
        self.h_draw_line = config.get('h_draw_line', False)
        self.draw_point = config.get('draw_point', True)
        if 'axisItems' in kwargs:
            self.__create_fitting_plotitem(*args, **kwargs)
        else:
            self.__create_fitting_plotitem(
                *args, axisItems={'bottom': self.__create_fitting_axis_item()}, **kwargs)

    def append_data(self, x: float, y: float) -> None:
        self.plotItem.plot_append(x, y)

    def __create_fitting_plotitem(self, *args, **kwargs):
        if self.plotting_style == PlotWidgetStyle.SLIDING_POINTER:
            self.plotItem = SlidingPointerPlotItem(timing_source=self.timing_source,
                                                   data_source=self.data_source,
                                                   cycle_size=self.cycle_size,
                                                   time_progress_line=self.time_progress_line,
                                                   v_draw_line=self.v_draw_line,
                                                   h_draw_line=self.h_draw_line,
                                                   draw_point=self.draw_point,
                                                   *args, **kwargs)
        elif self.plotting_style == PlotWidgetStyle.SCROLLING_PLOT:
            self.plotItem = ScrollingPlotItem(timing_source=self.timing_source,
                                              data_source=self.data_source,
                                              cycle_size=self.cycle_size,
                                              time_progress_line=self.time_progress_line,
                                              v_draw_line=self.v_draw_line,
                                              h_draw_line=self.h_draw_line,
                                              draw_point=self.draw_point,
                                              *args, **kwargs)
        self.setCentralItem(self.plotItem)

    def __create_fitting_axis_item(self):
        if self.plotting_style == PlotWidgetStyle.SLIDING_POINTER:
            return RelativeTimeAxisItem(orientation="bottom")
            # return TimeAxisItem(orientation="bottom")
        elif self.plotting_style == PlotWidgetStyle.SCROLLING_PLOT:
            return TimeAxisItem(orientation="bottom")
