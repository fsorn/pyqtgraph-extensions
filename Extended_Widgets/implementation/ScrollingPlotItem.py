from Extended_Widgets.interface.ExtendedPlotItem import ExtendedPlotItem
from Extended_Widgets.implementation.PlotItemUtils import PlotItemUtils
from typing import List, Dict

MAX_BUFFER_SIZE = 1000000


class ScrollingPlotItem(ExtendedPlotItem):

    """
    Displays data as a sliding pointer widget
    """

    def __init__(self, cycle_size: int = 100, *args, **kwargs):
        super(ScrollingPlotItem, self).__init__(*args, **kwargs)
        self.full_curve_buffer: Dict[str, List[float]] = {
            'x': [],
            'y': [],
        }
        self.cycle_size = cycle_size
        self.plot_data_item = self.plot()

    def plot_append(self, x, y):
        self.full_curve_buffer['x'].append(x)
        self.full_curve_buffer['y'].append(y)
        if len(self.full_curve_buffer['x']) > MAX_BUFFER_SIZE:
            self.full_curve_buffer['x'] = self.full_curve_buffer['x'][MAX_BUFFER_SIZE // 2:]
            self.full_curve_buffer['y'] = self.full_curve_buffer['y'][MAX_BUFFER_SIZE // 2:]

    def _handle_timing_update(self, timestamp: float) -> None:
        """
        Handle a update in the current time triggered by the timing source
        """
        super(ScrollingPlotItem, self)._handle_timing_update(timestamp)
        self._draw_time_line_decorator(timestamp=timestamp, position=timestamp)
        self.__update_curve_data_item()
        x = self.full_curve_buffer['x'][-1] if len(self.full_curve_buffer['x']) > 0 else timestamp
        y = self.full_curve_buffer['y'][-1] if len(self.full_curve_buffer['y']) > 0 else 0
        self._draw_plotting_position_decorators(x, y)

    def __update_curve_data_item(self):
        self.__update_cycle_numbers()
        # Calculate intersections with cycle start and cycle end
        ist = PlotItemUtils.intersect(self.full_curve_buffer, self.cycle_start)
        ien = PlotItemUtils.intersect(self.full_curve_buffer, self.cycle_end)
        curve_x = [] if ist['intersection'] is None else [ist['intersection']['x']]
        curve_y = [] if ist['intersection'] is None else [ist['intersection']['y']]
        if ien['intersection'] is None:
            # Add points from cycle start to newest appended point
            curve_x += [] if ist['first_after_index'] == -1 else self.full_curve_buffer['x'][ist['first_after_index']:]
            curve_y += [] if ist['first_after_index'] == -1 else self.full_curve_buffer['y'][ist['first_after_index']:]
        else:
            # Add points between cycle start and end
            curve_x += [] if ist['first_after_index'] == -1 else\
                self.full_curve_buffer['x'][ist['first_after_index']:ien['first_after_index']]
            curve_y += [] if ist['first_after_index'] == -1 else\
                self.full_curve_buffer['y'][ist['first_after_index']:ien['first_after_index']]
            # Add new point at cycle start created with clipping
            curve_x += [ien['intersection']['x']]
            curve_y += [ien['intersection']['y']]
        self.plot_data_item.setData({'x': curve_x, 'y': curve_y})

    def __update_cycle_numbers(self):
        self.cycle_start = self.last_timestamp - self.cycle_size
        self.cycle_end = self.last_timestamp

