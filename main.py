from Extended_Widgets.implementation.ExtendedPlotWidget import ExtendedPlotWidget as EPW
from Extended_Widgets.implementation.ExtendedPlotWidget import PlotWidgetStyle as PWS
from Connection.implementation.TimingSource import TimingSource
from Connection.implementation.DataSource import DataSource
import sys
from PyQt5.QtWidgets import *

# Example for usage of Sliding Pointer Widget



class MainWindow(QMainWindow):

    def __init__(self, app):
        super(MainWindow, self).__init__()
        self.app = app
        cycle_size_in_s = 10
        # Two Threads for Time and Data updates
        timing_source = TimingSource()
        data_source = DataSource()
        plot_config = {
            'cycle_size': cycle_size_in_s,
            'plotting_style': PWS.SLIDING_POINTER,
            'timestamp_plotting': True,
            'time_progress_line': True,
            'v_draw_line': False,
            'h_draw_line': False,
            'draw_point': True
        }
        self.plot = EPW(timing_source=timing_source, data_source=data_source, config=plot_config)
        self.show()
        self.resize(800, 600)
        main_container = QWidget()
        self.setCentralWidget(main_container)
        main_layout = QGridLayout()
        main_container.setLayout(main_layout)
        main_layout.addWidget(self.plot)


def run():
    app = QApplication(sys.argv)
    gui = MainWindow(app)
    sys.exit(app.exec_())

run()
