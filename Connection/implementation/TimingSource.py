from Connection.interface.UpdateSource import UpdateSource
from PyQt5.QtCore import QTimer, pyqtSignal

from datetime import datetime


class TimingSource(UpdateSource):

    def __init__(self, *args, **kwargs):
        super(TimingSource, self).__init__(*args, **kwargs)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.create_new_value)
        self.timer.start(1000/60)

    def create_new_value(self) -> None:
        self.TIME_SIGNAL.emit(datetime.now().timestamp())
