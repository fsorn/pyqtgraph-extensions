from Connection.interface.UpdateSource import UpdateSource
from PyQt5.QtCore import QTimer
from datetime import datetime
from typing import Dict
import numpy as np


class DataSource(UpdateSource):

    """
    Subscribe to DataSourceAdapter for receiving new data values
    """

    def __init__(self, *args, **kwargs):
        super(DataSource, self).__init__(*args, **kwargs)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.create_new_value)
        self.timer.start(1500)

    def create_new_value(self) -> None:
        offset = 0
        new_data = {
            'x': datetime.now().timestamp() + offset,
            'y': np.random.randint(0, 100, 1)[0]
        }
        self.DATA_SIGNAL.emit(new_data)
