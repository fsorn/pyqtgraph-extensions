from PyQt5.QtCore import QObject, pyqtSignal
from datetime import datetime


class UpdateSource(QObject):

    """
    Wrapper for two predefined signals for timing and data updates.
    """

    TIME_SIGNAL = pyqtSignal(float)
    DATA_SIGNAL = pyqtSignal(dict)

    def __init__(self, *args, **kwargs):
        super(UpdateSource, self).__init__(*args, **kwargs)

